let user = {
    info: {
        name: "alice",
        age: 2,
    },
}

let {name,age} = user.info;
console.log(name,age);

let produce = {
    names: "sony",
    price: 3,
};

let {names:produceName,price} = produce
console.log(produceName,price);


let colors = ["red","blue"];

let [c1,c2] = colors

console.log