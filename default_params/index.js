let tinhDiemTrungBinh = (toan = 0,ly = 0 ,hoa = 0 ) => {
    return (toan + ly + hoa) / 3;
};

let dTB1 = tinhDiemTrungBinh(5,5,5);
console.log(dTB1);

let dTB2 = tinhDiemTrungBinh(5,5);
console.log(dTB2);

let dTB3 = tinhDiemTrungBinh();
console.log(dTB3);